/**
 * Copyright Brenn Hill 2014
 * Uses semi-classical inheritance/functional inheritance.
 * http://elegantcode.com/2013/03/22/basic-javascript-prototypical-inheritance-vs-functional-inheritance/
 *
 * Note: performance of functional inheritance is substantially slower than pure prototype approaches, this is
 * not a performance sensitive app in the slightest.  This is also the easiest pattern to extend for JS/non-JS peeps.
 * You can also use this approach with extension factories to actually use prototype lookups, resulting in good performance
 * if you avoid using closure privacy.
 *
 * For performance test see:
 * http://jsperf.com/prototype-vs-closures/24
 * Licensed under GPL
 */
;(function( $, window, document, undefined ) {
    /**
     * Engine Engine constructor
     * @param settings
     * @constructor
     */
    var Engine = {};

    /**
     * Set Engine version.
     * @type {string}
     */
    Engine.version = "1.0";

    //tests if something is a function. Stolen from underscore.
    Engine.isFunction=function(obj) {
        return !!(obj && obj.constructor && obj.call && obj.apply);
    }


    /**
     * Engine.Engine constructor.
     * @constructor
     */
    Engine.Engine=function(settings){
        this.settings=settings;

        //holds the current Prompt.
        this.currentPrompt = {};

        //holds the last Prompt.
        this.lastPrompt;

        //What Prompt we are on.
        this.PromptNumber = 0;

        //whether or not user can advance to the next.  Updated based on the response from the ScoreKeeper
        this.canAdvance=false;
    };

    /**
     * Base class constructor to give type to objects.
     * @constructor
     */
    Engine.Timer = function(settings){
        this.settings=settings;
    };

    /**
     * Base constructor for Prompt.
     * A prompt is any instruction/screen for the user.
     * @constructor
     */
    Engine.Prompt = function(settings){
        this.settings=settings;
        this.type="default";
        this.payload={};
    };

    /**
     * PromptProvider abstract "class" builder.
     * @constructor
     *
     * Methods:
     * nextPrompt() provides the next Prompt.
     */
    Engine.PromptProvider = function(settings){
        this.settings=settings;
        /**
         * gets the next Prompt from the provider.
         */
        this.nextPrompt = function() {

        }
    };

    /**
     * Builds a ScoreKeeper object
     * Describes the core ScoreKeeper API expected by the Engine.js engine.
     * @constructor
     */
    Engine.ScoreKeeper = function(){
        /**
         * The current Prompt.
         */
        this.Prompt;

        /**
         * The current score, however that is defined for the scorekeeper.
         */
        this.score;

        /**
         * Sets the current Prompt.
         * @param q
         */
        this.setCurrentPrompt = function (Prompt) {
            this.Prompt = Prompt
        };

        /**
         * Abstract method.  Should return whether or not the Engine is finished.
         */
       this.isSequenceOver = function () {}

        /**
         * Returns a numeric score.
         * @returns {number}
         */
        this.getScore = function () {
            return this.score;
        }

        /**
         * Gets total results of the Engine as of the calling of the function.
         * @returns Total score as defied by the ScoreKeeper.
         */
        this.getResults = function () {};
    }

    /**
     * sets up prototype chain.
     * @param childObj
     * @param parentObj
     */
    Engine.extend =function(childObj, parentObj) {
        var tmpObj = function () {}
        tmpObj.prototype = parentObj.prototype;
        childObj.prototype = new tmpObj();
        childObj.prototype.constructor = childObj;
    }

    /**
     * Prompt factory. All providers should supply data to this. Can be built using an existing Prompt or an object with matching keys.
     * @param obj an object describing the Prompt.  Factory will provide defaults for items not set.
     *        object members: {type:{string}, payload:{object}, answers:{object}, correct:{object}, id:{string}
     * @returns {Engine.Prompt}
     */
    Engine.makePrompt = function (obj) {

        var q = new Engine.Prompt();
        //Prompt type.  Used to find a template to display it.
        q.type = obj.type || "default";
        /*
         The data to be passed to the template to display this.  Can involve multiple screens of text, links to video, etc,
         as long as template can handle it.
         */
        q.payload = obj.payload || {};
        /**
         * Array of possible answers and their point value.  Points values other than zero are considered valid.  incorrect answers are either zero or false.
         */
        q.answers = obj.answers || {};
        q.correct = obj.correct || "";
        q.id = obj.id || "none";
        return q;
    };


    /**
     * Builds a Engine timer.
     * @param settings
     * @returns {Engine.Timer}
     */
    Engine.makeTimer =function(settings) {
        var defaults = {
            updateInterval: 50,//update every 20th of a second
            timeLimit: 600000,
            updateFunc:function(){}
        }

        var timer = new Engine.Timer();


        var theEngine;

        var config = $.extend(defaults, settings);

        var originalLimit = config.timeLimit;

        var start = 0, elapsed = 0;

        var counter, timeout;

        timer.update = function() {
            elapsed =  new Date().getTime() - start;
            try {
                config.updateFunc(elapsed);
            }catch(e){
                console.log(e);
            }
        }

        /**
         * Starts the timer.
         */
        timer.start = function() {
           start = new Date().getTime();
           counter = setInterval(this.update, config.updateInterval);
           timeout = setTimeout(this.timesUp, config.timeLimit);
        }

        /**
         * Stops the timer, keeps remaining time.
         */
        timer.stop =function() {
            //keep accurate measure.
            this.update();
            clearTimeout(timeout);
            clearInterval(counter);
            config.timeLimit =  config.timeLimit - elapsed;
        }

        /**
         * Tells Engine that the time is over.
         */
        timer.timesUp = function() {
            clearTimeout(timeout);
            clearInterval(counter);
            if(theEngine && theEngine.timerUp) {
                theEngine.timerUp();
            }
        }

        /**
         * Gets the length of time the timer has gone.
         * @returns {number}
         */
        timer.getElapsed=function() {
            return elapsed;
        }

        /**
         * Gets the max length of the timer, start to finish
         * @returns {number}
         */
        timer.getMaxTime = function() {
            return originalLimit;
        }
        /**
         * sets interval for update of timer, in ms. You want at most 100 in most cases.
         * @param interval
         */
        timer.setInterval = function(interval) {
            config.interval = interval;
        }
        /**
         * set timelimit of Engine.  Only matters before timer starts.
         * @param time
         */
        timer.setTimeLimit=function(time) {
            config.timeLimit = time;
        }

        timer.setEngine = function(Engine) {
            theEngine = Engine;
        }

        return timer;
    };


    /**
     * Creates a PromptProvider
     * @param Prompts
     * @returns {Engine.PromptProvider}
     */
    Engine.makeBasicPromptProvider = function (Prompts) {
        var provider = new Engine.PromptProvider();

        /**
         * Hide Prompt list in closure.
         */
        var qs = Prompts;
        var max;
        if (qs) {
            max = qs.length;
        }
        var index = 0;
        provider.nextPrompt = function () {
            if (qs && max > 0) {
                var val = qs[index];
                index++;
                //if index=max we have gone through all Prompts, reset to beginning.
                if (index >= max) {
                    index = 0;
                }
                return Engine.makePrompt(val);
            } else {
                console.log("no Prompts exist");
                return {};
            }
        };
        return provider;
    };



    /**
     * Creates a Engine ScoreKeeper with minimal behavior.
     * @param settings
     * @returns {Engine.ScoreKeeper}
     */
    Engine.makeScoreKeeper = function(settings) {
        var _settings = $.extend({maxTries:10}, settings);
        var scorekeeper = new Engine.ScoreKeeper();
        scorekeeper.Prompt;
        scorekeeper.correct = 0;
        scorekeeper.incorrect = 0;
        scorekeeper.attempts = 0;
        scorekeeper.score = 0;

        scorekeeper.PromptsAnswered = 0;
        scorekeeper.totalPrompts = 0;
        scorekeeper.answered = false;

        /**
         * Sets the next Prompt to be evaluated.
         * Also resets whether or not a particular quesiton has been answered.
         * @param q
         */
        scorekeeper.setCurrentPrompt = function (q) {
            scorekeeper.Prompt = q;
            scorekeeper.answered = false;
        };

        /**
         * checks to see if Engine is over based on attempts
         */
        scorekeeper.isSequenceOver = function () {
            if (this.attempts>_settings.maxTries) {
                return true;
            }
            return false;
        }

        /**
         * Gets total results of the Engine as of the calling of the function.
         * @returns {{score: number, correct: number, incorrect: number, attempts: number}}
         */
        scorekeeper.getResults = function () {
            return {score: scorekeeper.score, correct: scorekeeper.correct, incorrect: scorekeeper.incorrect, attempts: scorekeeper.attempts};
        }

        return scorekeeper;
    }

    /**
     * Creates a basic ScoreKeeper
     * @param settings
     * @returns {Engine.ScoreKeeper}
     */
    Engine.makeBasicScoreKeeper = function(settings) {
        var scorekeeper = Engine.makeScoreKeeper(settings);
        /**
         * Evaluates an answer to the current Prompt.
         * @param a An answer to the set Prompt.
         * @returns {{correct: boolean, response: string or object, allowAdvance:boolean}}
         */
        scorekeeper.evaluateAnswer = function (a) {
            scorekeeper.attempts++;
            //If Prompt comes with a correct answer, use that, otherwise evaluate points.
            if (scorekeeper.Prompt && scorekeeper.Prompt.correct) {
                if (scorekeeper.Prompt.correct.label == a) {
                    if (!scorekeeper.answered) {
                        scorekeeper.score += scorekeeper.Prompt.correct.value;
                        scorekeeper.correct++;
                        scorekeeper.answered = true;
                    }
                    scorekeeper.answered = true;
                    return {correct: true, response: "correct!", allowAdvance: true}
                } else {
                    scorekeeper.incorrect++;
                    return {correct: false, response: "incorrect!", allowAdvance: false}
                }
            }
            //otherwise there might be multiple correct answers
            else if (scorekeeper.Prompt && scorekeeper.Prompt.answers) {
                if (scorekeeper.Prompt.answers[a]) {
                    if (scorekeeper.Prompt.answers[a].value) {
                        if (!answered) {
                            correct++
                            score += scorekeeper.Prompt.answers.a.value;
                            answered = true;
                        }
                        return {correct: true, response: "correct!", allowAdvance: true}
                    } else {
                        incorrect++;
                        return {correct: false, response: "incorrect!", allowAdvance: false}
                    }
                }
            }
        }

        /**
         * Gets checked after Prompts answered is incremented, so needs to be greater than total Prompts.
         */
        scorekeeper.isSequenceOver = function (onPrompt, totalPrompts) {
            if (onPrompt > totalPrompts) {
                return true;
            }
            return false;
        }

        return scorekeeper;
    };

    /**
     * Builds a renderer.
     * @param settings
     * @constructor
     */
    Engine.Renderer = function(settings){
        /**
         * Holds the rendering templates.
         */
        this.settings = settings;

        /**
         * Sets a reference back to the main Engine for this renderer.
         * @param Engine
         */
        this.setEngine=function(Engine) {
            this.Engine = Engine;
        }


       // this.renderer.advancePrompt();

        /**
         * Renders a Prompt.
         * @param Prompt
         */
        this.renderPrompt=function(Prompt) {
            $(this.Enginesettings.popupSelector).hide();
            if(Prompt && Prompt.payload) {
                console.log(Prompt.payload);
            } else {
                console.log("no payload");
            }
        };
    };

    /**
     * Makes a Engine renderer.
     * @param settings
     * @returns {Engine.Renderer}
     */
    Engine.makeHTMLRenderer = function(settings) {
        renderer = new Engine.Renderer(settings);
        renderer.templates= settings.prompttemplates;
        renderer.endscreen = settings.endScreen;
        renderer.target = settings.target;

        /**
         * Renders a Prompt.
         * @param Prompt
         */
        renderer.renderPrompt =function(Prompt) {
            $(this.settings.popupSelector).hide();
            var rendered;
            if(Prompt) {
                var type = Prompt.type || "default";
                var func = this.templates[type];
                if(Engine.isFunction(func)) {
                    rendered = func(Prompt);
                } else {
                    console.log("ERROR: no appropriate function for type "+type);
                    rendered = "ERROR LOADING TEMPLATE.";
                }
            } else {
                rendered ="NO Prompt TO RENDER";
            }
            $(this.target).html(rendered);
        };

        /**
         * Renders the end screen of the Engine.
         */
        renderer.renderEndOfEngine=function(finaldata) {
            $(this.settings.popupSelector).hide();
            var elapsed = 0;

            //If endscreen is set as a function, treat it as a template.
            var endscreen = "";

            //Send final data to callback.
            if(this.settings.endCallback && Engine.isFunction(this.settings.endCallback)) {
                this.settings.endCallback(finaldata);
            }

            /** find either a setting for endscreen or an "endScreen" template **/
            if(Engine.isFunction(this.endscreen)) {
                endscreen = this.endscreen(finaldata);
            } else if(Engine.isFunction(this.templates.endScreen)) {
                console.log("building endscreen")
                endscreen = this.templates.endScreen(finaldata);
            }
            //If endscreen is not a function, treat it as a selector for a static endscreen, get the html and render.
            else {
                console.log("building HTML endscreen")
                endscreen = $(this.templates.endScreen).html();
            }
            //Render final screen
            $(this.target).html(endscreen);
        }

        /**
         * hide the answer message.
         */
        renderer.hideAnswerMessage=function() {
            $(this.settings.popupSelector).hide();
        }

        /**
         *
         * @param Prompt
         * @param response
         */
        renderer.showAnswerMessage = function(Prompt, response) {
            var message;
            if(this.settings.popupRenderer && Engine.isFunction(this.settings.popupRenderer)) {
                try {
                    message = this.settings.popupRenderer(response);
                    console.log($(this.settings.popupSelector));
                    $(this.settings.popupSelector).html(message);
                    $(this.settings.popupSelector).show();
                }catch(e) {
                    console.log(e)
                }
            } else {
                $(this.settings.popupSelector).show();
            }
        };

        return renderer;
    }

    /**
     * Inherits from Engine.ScoreKeeper.
     * @param settings
     * @returns {*}
     * @constructor
     */
    Engine.AdditiveScoreKeeper=function(settings) {
        var scorekeeper = Engine.makeScoreKeeper(settings);
        return scorekeeper;
    }

    /**
     * Creates the core engine with defaults.
     * Settings is mandatory and should be used to set a PromptsProvider.  Otherwise a simple list of Prompts can be used as second argument.
     * TODO: detect args and see if first parameter is array. If so use as Prompt list and use default settings only.
     * @param settings
     * @param Prompts
     * @constructor
     */
    Engine.getEngine = function (settings, Prompts) {

        //Gives us a reference back to the main Engine engine object.
        var thisEngine =  new Engine.Engine();


        //let's us know if the Engine has been launched.
        var began = false;

        var defaults = {
            endCallback:            null,
            answerCallback:         null,
            usetimer:               true,
            timer:                  Engine.makeTimer(),
            timeLimit:              20000,
            endOnTimeUp:            false,
            numPrompts:           5,
            //if just a list of Prompts is provided, then build a default provider.
            PromptsProvider:    (function () {
                if (Prompts && (!settings || !settings.PromptsProvider)) {
                    return Engine.makeBasicPromptProvider(Prompts)
                }
                return undefined;
            })(),
            //A basic scorekeeper.
            scoreKeeper: (function(){
                if(!settings || !settings.scoreKeeper) {
                    return Engine.makeBasicScoreKeeper();
                }
            })(),
            renderer:null,

            PromptAdvancer:       {},
            //Whether or not to try a Prompt again.
            allowRetry:             false,
            EngineName:               "sample Enginee",
            //Where to inject templates.
            target:                 "#Engine",
            //the selector or
            popupSelector:          "#overlay",
            //the rendering template for popup success/failure messages.
            popupRenderer:          "Prompt submitted <a href='#' class='dismiss'>dismiss</a>",
            //the selector for the start screen.
            startScreen:            "#startScreen",
            //the selector for the end screen.
            endScreen:              "#endScreen",
            /**
             *  holds the template functions or ids.
             *  uses the Prompt type to find an appropriate template.  The two should match.
             *  E.g. if a quesiton has type "default", then templates[default] will be used to render
             *  Thus any function based rendering engine can be used as long as the right values are used.
             *  A custom Prompt provider will produce Prompts with appropriate payload/answers
             *  and a custom response evaluator can be used for handling the responses.
             */
            prompttemplates:            {"default": function (q) {
                console.log(q)
            }}
        };

        //Setup the settings based on defaults + overrides.
        thisEngine.Enginesettings = jQuery.extend(defaults, settings || {});

        //easy reference to target.
        thisEngine.target = thisEngine.Enginesettings.target;

        //Easy reference to templates.
        thisEngine.templates = thisEngine.Enginesettings.prompttemplates;

        //The configured Prompts provider
        thisEngine.PromptsProvider = thisEngine.Enginesettings.PromptsProvider;

        //sets the scorekeeper.
        thisEngine.scoreKeeper =  thisEngine.Enginesettings.scoreKeeper;

        //Reference to the timer.
        thisEngine.timer = thisEngine.Enginesettings.timer;

        //Engine rendererer.
        thisEngine.renderer = thisEngine.Enginesettings.renderer || Engine.makeHTMLRenderer(thisEngine.Enginesettings);
        thisEngine.renderer.setEngine(thisEngine);

        /**
         * Callback for the method being up.
         * @param time
         */
        thisEngine.timerUp=function(time) {
            if(this.Enginesettings.endOnTimeUp) {
                this.timer.stop();
                thisEngine.endEngine();
            }
        }

        /**
         * Ends the Engine. Passes options on to the renderer along with score data.
         * @param options
         */
        thisEngine.endEngine=function(options) {
            thisEngine.canAdvance = false;
            if(thisEngine.timer) {
                try {
                    this.timer.stop();
                    elapsed = this.timer.getElapsed();
                } catch (e) {
                    console.log(e);
                }
            }
            var finaldata = {
                results:this.scoreKeeper.getResults(),
                answered:this.numPrompts,
                maxPrompts:this.Enginesettings.maxPrompts,
                timeSpent:thisEngine.timer.getElapsed()
            };

            this.renderer.renderEndOfEngine(finaldata, options);
        }

        /**
         * Adds current score, current quesiton number, and max Prompts to dataObj
         * @param Prompt
         */
        function decorateCurrentPrompt(Prompt) {
            Prompt = Prompt || {};
            Prompt.currentScore = thisEngine.scoreKeeper.getScore();
            Prompt.currentPrompt = thisEngine.PromptNumber;
            Prompt.totalPrompts = thisEngine.Enginesettings.numPrompts;
        }


        /**
         * Advances to next Prompt.
         * @param data data to be used by the PromptProvider to get the next Prompt.
         */
        thisEngine.advancePrompt=function(data) {

            //If the scorekeeper said we could move on, get the next Prompt.  Otherwise, display the last one again.
            if(this.canAdvance) {
                this.lastPrompt = this.currentPrompt;
                this.currentPrompt = this.PromptsProvider.nextPrompt();
                this.PromptNumber++;
            }
            if(this.scoreKeeper.isSequenceOver(this.PromptNumber, thisEngine.Enginesettings.numPrompts)) {
                thisEngine.endEngine();
            } else {
                //Stop advancing until we get a correct answer according to the scorekeeper.
                this.canAdvance = false;
                decorateCurrentPrompt(this.currentPrompt);
                this.scoreKeeper.setCurrentPrompt(this.currentPrompt);
                this.renderer.renderPrompt(this.currentPrompt);
                //if Prompt callback is set, call it when we first get the new Prompt.
                if(this.currentPrompt && this.Enginesettings.PromptCallback && Engine.isFunction(this.Enginesettings.PromptCallback)) {
                    this.Enginesettings.PromptCallback(this.currentPrompt);
                }
            }
        }

        /**
         * Handle the response to the current answer.
         * Optionally call a callback.  Callback is
         * performed BEFORE the score is tabulated.
         * @param answer
         */
        thisEngine.submitAnswer = function(answer) {
            if(this.Enginesettings.answerCallback && Engine.isFunction(this.Enginesettings.answerCallback)) {
                this.Enginesettings.answerCallback(this.currentPrompt, answer);
            }
            var response = this.scoreKeeper.evaluateAnswer(answer);
            if(response.allowAdvance) {
                this.canAdvance=true;
            }
            this.renderer.showAnswerMessage(this.currentPrompt, response);
        };



        /**
         * Starts the Engine. Takes a data object that can be used to customize the start screen.
         * if settings.startScreen is a function, the function is called with data as it's first and only argument and the response is expected
         * to be html.  If settings.startScreen is not a function, it is expected to be a string which is treated as a selector.
         * The html retrieved using that selector will be used as the startscreen.
         * It's important that there is either a button or a timer that also calls beginEngine() to start showing Prompts.
         *
         * In either case, if data and a start callback is supplied, that callback is called with the data object as an argument.
         *
         * @param startData data to be used if the startScreen setting is a function/template.
         */
        thisEngine.loadStartScreen = function (data) {
            console.log("loading start screen");
            var html;
            if(data && this.Enginesettings.startCallback && Engine.isFunction(this.Enginesettings.startCallback)) {
              this.Enginesettings.startCallback(data);
            }
            if(Engine.isFunction(this.Enginesettings.startScreen)) {
              html=this.Enginesettings.startScreen(data);
            } else {
              html = $(this.Enginesettings.startScreen).html();
            }
            $(this.Enginesettings.target).html(html);
        }

        /**
         * Begins the actual QA.
         */
        thisEngine.runEngine = function(){
            if(!began) {
                began = true;
                this.canAdvance=true; //make sure user can get to first Prompt.
                if(this.Enginesettings.usetimer) {
                    this.timer.setEngine(thisEngine);
                    this.timer.setTimeLimit(this.Enginesettings.timeLimit);
                    this.timer.start();
                }
                this.advancePrompt();
            }
        };
        return thisEngine;
    };
    $.Engine = Engine;

})(jQuery, window, document);





