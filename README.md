# Engine.js Q/A Engine

A feature-rich quiz engine that should allow you to build and skin any multiple-choice style quizes, and Question/Answer sets, surveys, etc.  Support exists for different score keeping approaches, time based quizes, etc.

The core Engine code gives classes/constructors that can be extended to create new functionality.  As time goes on I'll add more advanced features, etc.

The intended use of the Engine.js code is either to create CMS managed quizes with the questions loaded via JSON, or a similar setup with network based QuestionProvider and ScoreKeeper objects that send to partner objects on the server (such as those running on node.js).

## Requirements
Engine.js only requires jQuery, and those requirements are very basic.  If needed a smaller selector engine (or no selector engine at all) could be substituted.  Work will continue in the future to have the quiz talk to a configured Renderer object and remove that dependency from the Engine engine itself.

## Use/Install
See the included index.html to see a basic quiz with timer.

###
Textures/patterns by <a href="http://subtlepatterns.com/">subtlepatterns.com</a>